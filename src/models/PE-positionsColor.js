import * as THREE from 'three';
export const positionsColor = (pixels) => {
  const positions = pixels.reduce((acc, value, index) => {
    return [
      ...acc,
      ...value.reduce((_acc, _value, _index) => {
        const { red, green, blue } = _value;
        const r = 0.299 * red;
        const g = 0.114 * green;
        const b = 0.587 * blue;
        const x = (_index - value.length / 2) * 0.1;
        const y = (index - value.length / 2) * 0.1;
        const z = r + g + b;
        return [..._acc, x, y, z];
      }, []),
    ];
  }, []);

  const colors = pixels.reduce((acc, value, index) => {
    return [
      ...acc,
      ...value.reduce((_acc, _value, _index) => {
        const { red, green, blue } = _value;
        const colorStringTwo = `rgb(${red}, ${green}, ${blue})`;
        const color = new THREE.Color(colorStringTwo).toArray();
        return [..._acc, ...color];
      }, []),
    ];
  }, []);

  return [new Float32Array(positions), new Float32Array(colors)];
};
