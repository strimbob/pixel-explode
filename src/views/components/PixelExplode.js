import * as THREE from 'three';
import React, { useRef } from 'react';
import { extend, useFrame } from '@react-three/fiber';

class DotMaterial extends THREE.ShaderMaterial {
  constructor() {
    super({
      transparent: false,
      uniforms: { size: { value: 10 }, scale: { value: -0.0 } },
      vertexShader: `
      uniform float size;
      uniform float scale;
      #include <common>
      #include <color_pars_vertex>
      #include <fog_pars_vertex>
      #include <morphtarget_pars_vertex>
      #include <logdepthbuf_pars_vertex>
 
      void main() {
        #include <color_vertex>
        #include <morphcolor_vertex>
        #include <begin_vertex>
        #include <morphtarget_vertex>
        gl_PointSize = size;
        float x = transformed.x;
        float y = transformed.y;
        float z = transformed.z;
        vec4 mvPosition = vec4( x,y,z * scale, 1.0 );
        mvPosition = modelViewMatrix * mvPosition;
        gl_Position = projectionMatrix * mvPosition;

        #ifdef USE_SIZEATTENUATION
        bool isPerspective = isPerspectiveMatrix( projectionMatrix );
        if ( isPerspective ) gl_PointSize *= ( scale / - mvPosition.z );
      #endif
      #include <logdepthbuf_vertex>
      #include <clipping_planes_vertex>
      #include <worldpos_vertex>
      #include <fog_vertex>
      }`,
      fragmentShader: `
        varying vec3 vColor;
        void main() {
          gl_FragColor = vec4(vColor, step(length(gl_PointCoord.xy - vec2(0.5)), 0.5));
        }`,
    });
  }
}

extend({ DotMaterial });

export const PixelExplode = ({ positions, colors }) => {
  const points = useRef(null);
  const material = useRef();

  useFrame(
    () => (material.current.uniforms.scale.value += 0.0001)
  );
  return (
    <points ref={points}>
      <bufferGeometry attach="geometry">
        <bufferAttribute
          attachObject={['attributes', 'position']}
          count={positions.length / 3}
          array={positions}
          itemSize={3}
        />
        <bufferAttribute
          attachObject={['attributes', 'color']}
          count={colors.length / 3}
          array={colors}
          itemSize={3}
        />
      </bufferGeometry>
      <dotMaterial vertexColors ref={material} />
    </points>
  );
};
