import * as THREE from 'three';

export const getPixels = ({ image }) => {
  return new Promise((reslove, reject) => {
    const textureLoader = new THREE.TextureLoader();

    textureLoader.load(image, (texture) => {
      const width = texture.image.width;
      const height = texture.image.height;
      const img = texture.image;
      const canvas = document.createElement('canvas');
      const ctx = canvas.getContext('2d');

      canvas.width = width;
      canvas.height = height;
      ctx.scale(1, -1);
      ctx.imageSmoothingEnabled = false;
      ctx.mozImageSmoothingEnabled = false;
      ctx.webkitImageSmoothingEnabled = false;
      ctx.drawImage(img, 0, 0, width, height * -1);
      const imgData = ctx.getImageData(0, 0, width, height);
      let rgbArray = [];
      for (let x = 0; x < width; x++) {
        let _rgb = [];
        for (let y = 0; y < height; y++) {
          const pixel = x + y * width;
          _rgb.push({
            red: imgData.data[pixel * 4],
            green: imgData.data[pixel * 4 + 1],
            blue: imgData.data[pixel * 4 + 2],
          });
        }
        rgbArray.push(_rgb);
      }
      console.log(rgbArray, 'rgbArray');
      reslove(rgbArray);
    });
  });
};
