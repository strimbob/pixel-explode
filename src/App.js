import React, { useState, useEffect } from 'react';
import { PixelExplode } from './views/components/PixelExplode';
import { OrbitControls } from '@react-three/drei';
import { Canvas } from '@react-three/fiber';
import image from './images/imageSmallerRectRotated.jpg';
import { getPixels } from './controller/PE-getPixels';
import { positionsColor } from './models/PE-positionsColor';
import './views/scss/index.scss';

export const App = (props) => {
  const [positions, setPositions] = useState();
  const [colors, setColors] = useState();

  useEffect(() => {
    const _getPixels = async () => {
      const _pixels = await getPixels({ image });
      const [_positions, _colors] = positionsColor(_pixels);
      setPositions(_positions);
      setColors(_colors);
    };
    _getPixels();
  }, []);

  return (
    <>
      <Canvas
        style={{ background: 'black', height: '100vh' }}
        orthographic={false}
        camera={{ zoom: 40, position: [3, 0, 100] }}
        raycaster={{ params: { Points: { threshold: 0.2 } } }}
      >
        <ambientLight />
        <pointLight position={[10, 10, 10]} />
        {positions && (
          <PixelExplode colors={colors} positions={positions} />
        )}
        <OrbitControls
          enableRotate={true}
          enableZoom={true}
          enablePan={true}
          minPolarAngle={0}
          maxPolarAngle={30}
        />
      </Canvas>
    </>
  );
};
